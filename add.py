# hardcoded [start]
#x = 5;
#y = 8;
# hardcoded [end]


# dynamic [start]
x = int(input("Enter number 1: "));
y = int(input("Enter number 2: "));
# dynamic [end]

z = x + y;

print("5 + 6 = 11");
print("{0} + {1} = {2}".format(x, y, z));
